FROM java:8-jre
MAINTAINER UNIFI f4w dev team <devs@facts4.work>

RUN apt-get update && apt-get install -y make python python-dev python-pip python-pillow python-sphinx python-sphinxcontrib.httpdomain --no-install-recommends && rm -rf /var/lib/apt/lists/*

RUN pip install sphinx-autobuild sphinx-rtd-theme
RUN pip install sphinxcontrib-sdedit --no-deps

CMD "bash"
